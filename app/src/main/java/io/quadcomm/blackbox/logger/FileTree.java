package io.quadcomm.blackbox.logger;

import android.util.Log;
import timber.log.Timber;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileTree {

    private final File file;

    public FileTree(File file) {
        this.file = file;
    }

    public void log(int priority, String tag, String message, Throwable t) {
        try (FileWriter writer = new FileWriter(file, true)) {
            writer.write((String.format("[ %s ][ %s ][ %s ] : %s%n", getDateAndTime(), tag, priorityToString(priority), message)));
            if (t != null) {
                writer.write(Log.getStackTraceString(t));
            }
        } catch (IOException e) {
            Timber.e(e, "Error writing to file");
        }
    }

    private String getDateAndTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(new Date());
    }

    private String priorityToString(int priority) {
        switch (priority) {
            case Log.VERBOSE: return "V";
            case Log.DEBUG: return "D";
            case Log.INFO: return "I";
            case Log.WARN: return "W";
            case Log.ERROR: return "E";
            default: return "UNKNOWN";
        }
    }
}
